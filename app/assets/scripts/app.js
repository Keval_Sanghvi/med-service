import "../styles/styles.css";
import "lazysizes";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";
import ChangeImage from "./modules/ChangeImage";
import $ from 'jquery';

let mobileMenu = new MobileMenu();

new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

new SmoothScroll();

new ActiveLinks();

new Modal();

new ChangeImage();

if(module.hot) {
    module.hot.accept();
}
// console.log("Hello");