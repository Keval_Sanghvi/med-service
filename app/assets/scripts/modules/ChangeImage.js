import $ from 'jquery';

class ChangeImage {
    constructor() {
        this.images = document.querySelectorAll("#departments .service-box");
        this.events();
    }

    events() {
        for (let i of this.images) {
            i.addEventListener('mouseover', this.changeWhite);
            i.addEventListener('mouseout', this.changeBlue);
        }
    }

    changeWhite() {
        let dataSrc = this.children[0].getAttribute("data-src");
        let newDataSrc = dataSrc.replace('blue', 'white');
        this.children[0].src = newDataSrc;
    }

    changeBlue() {
        let dataSrc = this.children[0].getAttribute("data-src");
        let newDataSrc = dataSrc.replace('white', 'blue');
        this.children[0].src = newDataSrc;
    }
}

export default ChangeImage;
